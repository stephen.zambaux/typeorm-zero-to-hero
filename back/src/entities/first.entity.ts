import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Second } from "./second.entity";

@Entity()
export class First extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  private _name: string;

  @Column()
  public get name(): string {
    return this._name;
  }
  public set name(value: string) {
    this._name = value;
  }

  private _second: Second;
  @ManyToOne(() => Second)
  public get second(): Second {
    return this._second;
  }
  public set second(value: Second) {
    this._second = value;
  }
}
