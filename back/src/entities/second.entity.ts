import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Second extends BaseEntity{

    @PrimaryGeneratedColumn()
    public id: number;

    private _name: string;

    @Column()
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }


}