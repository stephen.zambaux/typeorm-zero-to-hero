import { createConnection } from "typeorm"


async function run(){

    const connection = await createConnection(
        {
        "name": "default",
        "type": "postgres",
        "host": "localhost",
        "port": 54320,
        "username": "postgres",
        "logging": "all",
        "password": "postgres",
        "database": "app.v0",
        "entities": ["./dist/**/*.entity.js"],
        "synchronize": true
      })
    

}


run()